import os
import sys
import subprocess
import sqlite3 
from flask import Flask
from flask import render_template
from flask import request
from flask import url_for



app = Flask(__name__)
Port=8080
Host='0.0.0.0'
Debug=True



@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/enternew')
def newrec():
    return render_template('students.html')


@app.route('/addrec',methods = ['POST', 'GET'])
def addrec():
	if request.method == 'POST':
		try:
			nm = request.form['nm']
			addr = request.form['addr']
			city = request.form['city']
			pin = request.form['pin']
			
			with sql.conect('database.db') as con:
				cur = con.cursor()
				cur.execute('INSERT INTO students (name,addr,city,pin) VALUES (?,?,?,?)',(nm,addr,city,pin))
				
				con.commit()
				msg = 'Record successfully added'
		except:
			con.rollback()
			msg = 'Error in insert operation'
		
		finally:
			return render_template('results.html',msg = msg)
			con.close()
			
			
@app.route('/list')
def list():
	con = sqlite3.connect('database.db')
	con.row_factory = sqlite3.Row
	
	cur = con.cursor()
	cur.execute('SELECt * from students')
	
	rows = cur.fetchall()
	return render_template('list.html', rows = rows)


if __name__ == "__main__":
	app.run(debug=Debug,host=Host,port=Port)
